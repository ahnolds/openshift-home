from flask      import Flask, request
from os         import environ
from os.path    import join
from subprocess import check_call, check_output, PIPE, Popen

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, world!\nThis message fetched from GitLab (Version 4.7)'

# Trigger an update of the code (note that this only accepts POSTs)
@app.route('/trigger-update', methods=['POST'])
def trigger_update():
    # Get the json from the request
    try:
        body = request.get_json()
    except BadRequest:
        return
    # Check if there is a ref specified
    if 'ref' in body:
        ref = body['ref']
    else:
        return
    # Check if there is a before specified
    if 'before' in body:
        before = body['before']
    else:
        return
    # Check if there is an after specified
    if 'after' in body:
        after = body['after']
    else:
        return

    # Combine the before, after, and ref values into a single line to pass to the hooks
    hook_line = '{} {} {}'.format(before, after, ref)
    print(hook_line)

    # Get the current revision and make sure it matches before
    git_dir = join(environ['OPENSHIFT_HOMEDIR'], 'git', environ['OPENSHIFT_APP_NAME'] + '.git')
    current = check_output(['git', 'rev-parse', 'HEAD'], cwd=git_dir).strip()
    print(current)
    if current != before:
        return

    # Call git fetch
    deploy_branch = environ['OPENSHIFT_DEPLOYMENT_BRANCH']
    check_call(['git', 'fetch', 'gitlab', deploy_branch + ':' + deploy_branch], cwd=git_dir)
    print('\n\n\n')

    # Call the prereceive hook
    prerec = Popen(['gear', 'prereceive'], stdin=PIPE)
    prerec.communicate(hook_line)
    if prerec.returncode != 0:
        # Failure
        return

    print('\n\n\n')
    # Call the postreceive hook
    postrec = Popen(['gear', 'postreceive'], stdin=PIPE)
    postrec.communicate(hook_line)
    if postrec.returncode != 0:
        # Failure
        return
    print('\n\n\n')

if __name__ == '__main__':
    app.run()
