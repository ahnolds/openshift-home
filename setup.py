from setuptools import setup

setup(name='AhnoldsHome',
      version='1.0',
      description='Ahnolds OpenShift Homepage',
      author='Alec C',
      author_email='ahnolds@gmail.com',
      url='http://home-ahnolds.rhcloud.com',
      install_requires=['flask', 'requests[security]'],
     )
